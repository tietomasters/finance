package com.respondlang.finance.dao;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.respondlang.finance.configuration.TestConfig;
import com.respondlang.finance.model.StudentsGroup;
import com.respondlang.finance.model.Teacher;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class,loader=AnnotationConfigContextLoader.class)
@Transactional
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class StudentsGroupDaoTest {

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Autowired
	private GenericDAO<StudentsGroup> studentsGroupDAO;

	@Test
	public void testAddGroup() {
		setupMockData();

		StudentsGroup group1 = new StudentsGroup(entityManager.find(Teacher.class, 1), "One");
		StudentsGroup group2 = new StudentsGroup(entityManager.find(Teacher.class, 1), "Two");

		studentsGroupDAO.add(group1);
		studentsGroupDAO.add(group2);

		StudentsGroup result = entityManager.find(StudentsGroup.class, 2);
		assertTrue(group2.equals(result));
		result = entityManager.find(StudentsGroup.class, 1);
		assertTrue(group1.equals(result));
	}

	@Test
	public void testUpdateGroup() {
		setupMockData();

		StudentsGroup group1 = new StudentsGroup(entityManager.find(Teacher.class, 1), "One");
		entityManager.persist(group1);
		StudentsGroup update = entityManager.find(StudentsGroup.class, 1);
		update.setGroupName("Two");

		studentsGroupDAO.update(update);

		StudentsGroup result = entityManager.find(StudentsGroup.class, 1);
		assertTrue(result.getGroupName().equals("Two"));
	}

	@Test
	public void testListGroups() {
		setupMockData();

		StudentsGroup group1 = new StudentsGroup(entityManager.find(Teacher.class, 1), "One");
		StudentsGroup group2 = new StudentsGroup(entityManager.find(Teacher.class, 1), "Two");
		entityManager.persist(group1);
		entityManager.persist(group2);

		List<StudentsGroup> teachersList = studentsGroupDAO.list(StudentsGroup.class);

		assertTrue(teachersList.contains(group1));
		assertTrue(teachersList.contains(group2));
	}

	@Test
	public void testGetGroupById() {
		setupMockData();

		StudentsGroup group1 = new StudentsGroup(entityManager.find(Teacher.class, 1), "One");
		StudentsGroup group2 = new StudentsGroup(entityManager.find(Teacher.class, 1), "Two");
		entityManager.persist(group1);
		entityManager.persist(group2);

		StudentsGroup result = studentsGroupDAO.getById(StudentsGroup.class, 2);
		assertTrue(result.equals(group2));

		result = studentsGroupDAO.getById(StudentsGroup.class, 1);
		assertTrue(result.equals(group1));
	}

	@Test
	public void testRemoveGroup() {
		setupMockData();

		StudentsGroup group1 = new StudentsGroup(entityManager.find(Teacher.class, 1), "One");
		entityManager.persist(group1);

		studentsGroupDAO.remove(group1);

		try {
			entityManager.find(StudentsGroup.class, 1);
		} catch(EntityNotFoundException exception) {
			assert(true);
		}
	}

	public void setupMockData() {
		Teacher teacher = new Teacher("John", "Doe");
		entityManager.persist(teacher);
	}
}

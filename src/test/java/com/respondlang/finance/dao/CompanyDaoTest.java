package com.respondlang.finance.dao;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.respondlang.finance.configuration.TestConfig;
import com.respondlang.finance.model.Company;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class,loader=AnnotationConfigContextLoader.class)
@Transactional
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class CompanyDaoTest {

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Autowired
	private GenericDAO<Company> companyDAO;

	@Test
	public void testAddCompany() {
		Company company1 = new Company("Acme", "Main St.", "123456789", 120, 50);
		Company company2 = new Company("Expol", "Big St.", "987654321", 125, 50);

		companyDAO.add(company1);
		companyDAO.add(company2);
		Company result = entityManager.find(Company.class, 2);
		assertTrue(company2.equals(result));
		result = entityManager.find(Company.class, 1);
		assertTrue(company1.equals(result));
	}

	@Test
	public void testUpdateCompany() {
		Company company1 = new Company("Acme", "Main St.", "123456789", 120, 50);

		entityManager.persist(company1);
		Company update = entityManager.find(Company.class, 1);
		update.setName("Expol");

		companyDAO.update(update);

		Company result = entityManager.find(Company.class, 1);
		assertTrue(result.getName().equals("Expol"));
	}

	@Test
	public void testListComapnies() {
		Company company1 = new Company("Acme", "Main St.", "123456789", 120, 50);
		Company company2 = new Company("Expol", "Big St.", "987654321", 125, 50);

		entityManager.persist(company1);
		entityManager.persist(company2);

		List<Company> companiesList = companyDAO.list(Company.class);

		assertTrue(companiesList.contains(company1));
		assertTrue(companiesList.contains(company2));
	}

	@Test
	public void testGetStudentById() {
		Company company1 = new Company("Acme", "Main St.", "123456789", 120, 50);
		Company company2 = new Company("Expol", "Big St.", "987654321", 125, 50);

		entityManager.persist(company1);
		entityManager.persist(company2);

		Company result = companyDAO.getById(Company.class, 2);
		assertTrue(result.equals(company2));

		result = companyDAO.getById(Company.class, 1);
		assertTrue(result.equals(company1));
	}

	@Test
	public void testRemoveCompany() {
		Company company1 = new Company("Acme", "Main St.", "123456789", 120, 50);
		entityManager.persist(company1);

		companyDAO.remove(company1);

		try {
			entityManager.find(Company.class, 1);
		} catch(EntityNotFoundException exception) {
			assert(true);
		}
	}
}

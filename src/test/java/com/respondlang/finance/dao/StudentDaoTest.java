package com.respondlang.finance.dao;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.respondlang.finance.configuration.TestConfig;
import com.respondlang.finance.model.Company;
import com.respondlang.finance.model.SettlementType;
import com.respondlang.finance.model.Student;
import com.respondlang.finance.model.StudentsGroup;
import com.respondlang.finance.model.Teacher;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class,loader=AnnotationConfigContextLoader.class)
@Transactional
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class StudentDaoTest {

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Autowired
	private GenericDAO<Student> studentDAO;

	@Test
	public void testAddStudent() {
		setupMockData();

		Student student1 = new Student(entityManager.find(Company.class, 1),
				entityManager.find(SettlementType.class, 1),
				entityManager.find(StudentsGroup.class, 1),
				"Mark",
				"Smith");
		Student student2 = new Student(entityManager.find(Company.class, 1),
				entityManager.find(SettlementType.class, 1),
				entityManager.find(StudentsGroup.class, 1),
				"Joe",
				"Black");

		studentDAO.add(student1);
		studentDAO.add(student2);
		Student result = entityManager.find(Student.class, 2);
		assertTrue(student2.equals(result));
		result = studentDAO.getById(Student.class, 1);
		assertTrue(student1.equals(result));
	}

	@Test
	public void testUpdateStudent() {
		setupMockData();

		Student student1 = new Student(entityManager.find(Company.class, 1),
				entityManager.find(SettlementType.class, 1),
				entityManager.find(StudentsGroup.class, 1),
				"Mark",
				"Smith");

		entityManager.persist(student1);
		Student update = entityManager.find(Student.class, 1);
		update.setLastName("Jones");

		studentDAO.update(update);

		Student result = entityManager.find(Student.class, 1);
		assertTrue(result.getLastName().equals("Jones"));
	}

	@Test
	public void testListStudents() {
		setupMockData();

		Student student1 = new Student(entityManager.find(Company.class, 1),
				entityManager.find(SettlementType.class, 1),
				entityManager.find(StudentsGroup.class, 1),
				"Mark",
				"Smith");
		Student student2 = new Student(entityManager.find(Company.class, 1),
				entityManager.find(SettlementType.class, 1),
				entityManager.find(StudentsGroup.class, 1),
				"Joe",
				"Black");
		entityManager.persist(student1);
		entityManager.persist(student2);

		List<Student> studentsList = studentDAO.list(Student.class);

		assertTrue(studentsList.contains(student1));
		assertTrue(studentsList.contains(student2));
	}

	@Test
	public void testGetStudentById() {
		setupMockData();

		Student student1 = new Student(entityManager.find(Company.class, 1),
				entityManager.find(SettlementType.class, 1),
				entityManager.find(StudentsGroup.class, 1),
				"Mark",
				"Smith");
		Student student2 = new Student(entityManager.find(Company.class, 1),
				entityManager.find(SettlementType.class, 1),
				entityManager.find(StudentsGroup.class, 1),
				"Joe",
				"Black");

		entityManager.persist(student1);
		entityManager.persist(student2);

		Student result = studentDAO.getById(Student.class, 1);
		assertTrue(result.equals(student1));

		result = studentDAO.getById(Student.class, 2);
		assertTrue(result.equals(student2));

	}

	@Test
	public void testRemoveStudent() {
		setupMockData();

		Student student1 = new Student(entityManager.find(Company.class, 1),
				entityManager.find(SettlementType.class, 1),
				entityManager.find(StudentsGroup.class, 1),
				"Mark",
				"Smith");

		entityManager.persist(student1);

		studentDAO.remove(student1);

		try {
			entityManager.find(Student.class, 1);
		} catch(EntityNotFoundException exception) {
			assert(true);
		}
	}

	public void setupMockData() {
		Company company = new Company("Acme", "Main St.", "123456789", 120, 50);
		entityManager.persist(company);

		SettlementType settlementType = new SettlementType("Own");
		entityManager.persist(settlementType);

		Teacher teacher = new Teacher("John", "Doe");
		entityManager.persist(teacher);

		StudentsGroup group = new StudentsGroup(teacher, "One");
		entityManager.persist(group);
	}
}

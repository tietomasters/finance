package com.respondlang.finance.dao;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.respondlang.finance.configuration.TestConfig;
import com.respondlang.finance.model.SettlementType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class,loader=AnnotationConfigContextLoader.class)
@Transactional
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class SettlementTypeDaoTest {

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Autowired
	private GenericDAO<SettlementType> settlementTypeDAO;

	@Test
	public void testAddSettlementType() {
		SettlementType settlementType1 = new SettlementType("Self");
		SettlementType settlementType2 = new SettlementType("BTB");

		settlementTypeDAO.add(settlementType1);
		settlementTypeDAO.add(settlementType2);

		SettlementType result = entityManager.find(SettlementType.class, 2);
		assertTrue(settlementType2.equals(result));
		result = entityManager.find(SettlementType.class, 1);
		assertTrue(settlementType1.equals(result));
	}

	@Test
	public void testUpdateSettlementType() {
		SettlementType settlementType1 = new SettlementType("Self");

		entityManager.persist(settlementType1);
		SettlementType update = entityManager.find(SettlementType.class, 1);
		update.setName("BTB");

		settlementTypeDAO.update(update);

		SettlementType result = entityManager.find(SettlementType.class, 1);
		assertTrue(result.getName().equals("BTB"));
	}

	@Test
	public void testListSettlementTypes() {
		SettlementType settlementType1 = new SettlementType("Self");
		SettlementType settlementType2 = new SettlementType("BTB");

		entityManager.persist(settlementType1);
		entityManager.persist(settlementType2);

		List<SettlementType> companiesList = settlementTypeDAO.list(SettlementType.class);

		assertTrue(companiesList.contains(settlementType1));
		assertTrue(companiesList.contains(settlementType2));
	}

	@Test
	public void testGetSettlementTypeById() {
		SettlementType settlementType1 = new SettlementType("Self");
		SettlementType settlementType2 = new SettlementType("BTB");

		entityManager.persist(settlementType1);
		entityManager.persist(settlementType2);

		SettlementType result = settlementTypeDAO.getById(SettlementType.class, 2);
		assertTrue(result.equals(settlementType2));

		result = settlementTypeDAO.getById(SettlementType.class, 1);
		assertTrue(result.equals(settlementType1));
	}

	@Test
	public void testRemoveSettlementType() {
		SettlementType settlementType1 = new SettlementType("Self");
		entityManager.persist(settlementType1);

		settlementTypeDAO.remove(settlementType1);

		try {
			entityManager.find(SettlementType.class, 1);
		} catch(EntityNotFoundException exception) {
			assert(true);
		}
	}
}

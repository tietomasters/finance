package com.respondlang.finance.dao;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.respondlang.finance.configuration.TestConfig;
import com.respondlang.finance.model.Teacher;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class,loader=AnnotationConfigContextLoader.class)
@Transactional
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class TreacherDaoTest {

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Autowired
	private GenericDAO<Teacher> teacherDAO;

	@Test
	public void testAddTeacher() {
		Teacher teacher1 = new Teacher("John", "Bell");
		Teacher teacher2 = new Teacher("Jake", "Horn");

		teacherDAO.add(teacher1);
		teacherDAO.add(teacher2);

		Teacher result = entityManager.find(Teacher.class, 2);
		assertTrue(teacher2.equals(result));
		result = entityManager.find(Teacher.class, 1);
		assertTrue(teacher1.equals(result));
	}

	@Test
	public void testUpdateTeacher() {
		Teacher teacher1 = new Teacher("John", "Bell");
		entityManager.persist(teacher1);
		Teacher update = entityManager.find(Teacher.class, 1);
		update.setFirstName("Lenny");

		teacherDAO.update(update);

		Teacher result = entityManager.find(Teacher.class, 1);
		assertTrue(result.getFirstName().equals("Lenny"));
	}

	@Test
	public void testListTeachers() {
		Teacher teacher1 = new Teacher("John", "Bell");
		Teacher teacher2 = new Teacher("Jake", "Horn");
		entityManager.persist(teacher1);
		entityManager.persist(teacher2);

		List<Teacher> teachersList = teacherDAO.list(Teacher.class);

		assertTrue(teachersList.contains(teacher1));
		assertTrue(teachersList.contains(teacher2));
	}

	@Test
	public void testGetTeacherById() {
		Teacher teacher1 = new Teacher("John", "Bell");
		Teacher teacher2 = new Teacher("Jake", "Horn");
		entityManager.persist(teacher1);
		entityManager.persist(teacher2);

		Teacher result = teacherDAO.getById(Teacher.class, 2);
		assertTrue(result.equals(teacher2));

		result = teacherDAO.getById(Teacher.class, 1);
		assertTrue(result.equals(teacher1));
	}

	@Test
	public void testRemoveTeacher() {
		Teacher teacher1 = new Teacher("John", "Bell");
		entityManager.persist(teacher1);

		teacherDAO.remove(teacher1);

		try {
			entityManager.find(Teacher.class, 1);
		} catch(EntityNotFoundException exception) {
			assert(true);
		}
	}
}

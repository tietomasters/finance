package com.respondlang.finance.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.respondlang.finance.configuration.TestConfig;
import com.respondlang.finance.model.Attendance;
import com.respondlang.finance.model.Company;
import com.respondlang.finance.model.SettlementType;
import com.respondlang.finance.model.Student;
import com.respondlang.finance.model.StudentsGroup;
import com.respondlang.finance.model.Teacher;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class,loader=AnnotationConfigContextLoader.class)
@Transactional
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class AttendanceDaoTest {

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Autowired
	private GenericDAO<Attendance> attendanceDAO;

	@Test
	public void testAddAttendance() {
		setupMockData();

		Attendance attendance1 = new Attendance(entityManager.find(Student.class, 1),
				(int) new Date().getTime());
		Attendance attendance2 = new Attendance(entityManager.find(Student.class, 1),
				(int) new Date().getTime() + 100);

		attendanceDAO.add(attendance1);
		attendanceDAO.add(attendance2);

		Attendance result = entityManager.find(Attendance.class, 2);
		assertTrue(attendance2.equals(result));
		result = entityManager.find(Attendance.class, 1);
		assertTrue(attendance1.equals(result));
	}

	@Test
	public void testUpdateAttendance() {
		setupMockData();

		Attendance attendance1 = new Attendance(entityManager.find(Student.class, 1),
				(int) new Date().getTime());
		entityManager.persist(attendance1);
		Attendance update = entityManager.find(Attendance.class, 1);
		update.setDate(666);

		attendanceDAO.update(update);

		Attendance result = entityManager.find(Attendance.class, 1);
		assertTrue(result.getDate() == 666);
	}

	@Test
	public void testListAttendances() {
		setupMockData();

		Attendance attendance1 = new Attendance(entityManager.find(Student.class, 1),
				(int) new Date().getTime());
		Attendance attendance2 = new Attendance(entityManager.find(Student.class, 1),
				(int) new Date().getTime() + 100);
		entityManager.persist(attendance1);
		entityManager.persist(attendance2);

		List<Attendance> teachersList = attendanceDAO.list(Attendance.class);

		assertTrue(teachersList.contains(attendance1));
		assertTrue(teachersList.contains(attendance2));
	}

	@Test
	public void testGetAttendanceById() {
		setupMockData();

		Attendance attendance1 = new Attendance(entityManager.find(Student.class, 1),
				(int) new Date().getTime());
		Attendance attendance2 = new Attendance(entityManager.find(Student.class, 1),
				(int) new Date().getTime() + 100);
		entityManager.persist(attendance1);
		entityManager.persist(attendance2);

		Attendance result = attendanceDAO.getById(Attendance.class, 2);
		assertTrue(result.equals(attendance2));

		result = attendanceDAO.getById(Attendance.class, 1);
		assertTrue(result.equals(attendance1));
	}

	@Test
	public void testRemoveAttendance() {
		setupMockData();

		Attendance attendance1 = new Attendance(entityManager.find(Student.class, 1),
				(int) new Date().getTime());
		entityManager.persist(attendance1);

		attendanceDAO.remove(attendance1);

		try {
			entityManager.find(Attendance.class, 1);
		} catch(EntityNotFoundException exception) {
			assert(true);
		}
	}

	public void setupMockData() {
		Company company = new Company("Acme", "Main St.", "123456789", 120, 50);
		entityManager.persist(company);

		SettlementType settlementType = new SettlementType("Own");
		entityManager.persist(settlementType);

		Teacher teacher = new Teacher("John", "Doe");
		entityManager.persist(teacher);

		StudentsGroup group = new StudentsGroup(teacher, "One");
		entityManager.persist(group);

		Student student = new Student(entityManager.find(Company.class, 1),
				entityManager.find(SettlementType.class, 1),
				entityManager.find(StudentsGroup.class, 1),
				"Mark",
				"Smith");
		entityManager.persist(student);
	}
}

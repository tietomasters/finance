-- ************************************** [teacher]

CREATE TABLE teacher
(
 `teacher_id` INT AUTO_INCREMENT  NOT NULL ,
 `first_name` VARCHAR(50) NOT NULL ,
 `last_name`  VARCHAR(50) NOT NULL ,

 CONSTRAINT `pk_86` PRIMARY KEY (`teacher_id` ASC)
);



-- ************************************** [company]

CREATE TABLE company
(
 `company_id`   INT AUTO_INCREMENT  NOT NULL ,
 `name`         VARCHAR(100) NOT NULL ,
 `address`      VARCHAR(100) NOT NULL ,
 `NIP`          VARCHAR(10) NOT NULL ,
 `hour_rate`    INT NOT NULL ,
 `refund_ratio` INT NOT NULL ,

 CONSTRAINT `pk_54` PRIMARY KEY (`company_id` ASC)
);



-- ************************************** [settlement_type]

CREATE TABLE settlement_type
(
 `name`          VARCHAR(50) NOT NULL ,
 `description`   VARCHAR(100) NULL ,
 `settlement_id` INT AUTO_INCREMENT  NOT NULL ,

 CONSTRAINT `pk_14` PRIMARY KEY (`settlement_id` ASC)
);



-- ************************************** [group]

CREATE TABLE students_group
(
 `group_id`   INT AUTO_INCREMENT  NOT NULL ,
 `group_name` VARCHAR(30) NOT NULL ,
 `teacher_id` INT NOT NULL ,

 CONSTRAINT `pk_77` PRIMARY KEY (`group_id` ASC),
 CONSTRAINT `fk_90` FOREIGN KEY (`teacher_id`)
  REFERENCES teacher(`teacher_id`)
);


-- SKIP Index: [fkIdx_90]


-- ************************************** [student]

CREATE TABLE student
(
 `student_id`      INT AUTO_INCREMENT  NOT NULL ,
 `first_name`      VARCHAR(50) NOT NULL ,
 `last_name`       VARCHAR(50) NOT NULL ,
 `company_name`    VARCHAR(100) NULL ,
 `company_address` VARCHAR(100) NULL ,
 `settlement_id`   INT NOT NULL ,
 `company_id`      INT NOT NULL ,
 `group_id`        INT NOT NULL ,

 CONSTRAINT `pk_5` PRIMARY KEY (`student_id` ASC),
 CONSTRAINT `fk_48` FOREIGN KEY (`settlement_id`)
  REFERENCES settlement_type(`settlement_id`),
 CONSTRAINT `fk_62` FOREIGN KEY (`company_id`)
  REFERENCES company(`company_id`),
 CONSTRAINT `fk_80` FOREIGN KEY (`group_id`)
  REFERENCES students_group(`group_id`)
);


-- SKIP Index: [fkIdx_48]

-- SKIP Index: [fkIdx_62]

-- SKIP Index: [fkIdx_80]


-- ************************************** [attendance]

CREATE TABLE attendance
(
 `attendance_id` INT AUTO_INCREMENT  NOT NULL ,
 `date`          INT NOT NULL ,
 `student_id`    INT NOT NULL ,

 CONSTRAINT `pk_68` PRIMARY KEY (`attendance_id` ASC),
 CONSTRAINT `fk_71` FOREIGN KEY (`student_id`)
  REFERENCES student(`student_id`)
);


-- SKIP Index: [fkIdx_71]
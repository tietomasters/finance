package com.respondlang.finance.configuration;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.respondlang.finance.dao.GenericDAO;
import com.respondlang.finance.dao.GenericDAOImpl;
import com.respondlang.finance.model.Attendance;
import com.respondlang.finance.model.Company;
import com.respondlang.finance.model.SettlementType;
import com.respondlang.finance.model.Student;
import com.respondlang.finance.model.StudentsGroup;
import com.respondlang.finance.model.Teacher;

@Configuration
@Profile("test")
@EnableTransactionManagement
public class TestConfig {

	private static final String MODEL_PACKAGE = "com.respondlang.finance.model";

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setDataSource(dataSource());
		entityManagerFactory.setJpaDialect(new HibernateJpaDialect());
		entityManagerFactory.setPackagesToScan(MODEL_PACKAGE);
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
		entityManagerFactory.setJpaProperties(additionalProperties());
		return entityManagerFactory;
	}

	@Bean
	public DataSource dataSource() {
		return new EmbeddedDatabaseBuilder().
				setType(EmbeddedDatabaseType.H2).
				addScript("create_schema.sql").
				addScript("create_db.sql").
				build();
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}

	Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", "create");
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		return properties;
	}

	@Bean
	public GenericDAO<Student> StudentDAO() {
		return new GenericDAOImpl<Student>();
	}

	@Bean
	public GenericDAO<Company> CompanyDAO() {
		return new GenericDAOImpl<Company>();
	}

	@Bean
	public GenericDAO<Attendance> AttendanceDAO() {
		return new GenericDAOImpl<Attendance>();
	}

	@Bean
	public GenericDAO<SettlementType> SettlementTypeDAO() {
		return new GenericDAOImpl<SettlementType>();
	}

	@Bean
	public GenericDAO<StudentsGroup> StudentsGroupDAO() {
		return new GenericDAOImpl<StudentsGroup>();
	}

	@Bean
	public GenericDAOImpl<Teacher> TeacherDAO() {
		return new GenericDAOImpl<Teacher>();
	}

}

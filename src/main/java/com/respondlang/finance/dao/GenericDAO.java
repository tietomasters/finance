package com.respondlang.finance.dao;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import com.respondlang.finance.model.Student;

public interface GenericDAO<T> {

	/**
	 * Create a new <T> entity record in the database.
	 *
	 * @param entity <T> to be created.
	 */
	public void add(T entity);

	/**
	 * Update existing <T> record in the database.
	 *
	 * @param entity <T> to be updated.
	 */
	public void update(T entity);

	/**
	 * List all <T> records in the database.
	 * @param c Class<T> to be listed
	 * @return a list of all <T> in database
	 */
	public List<T> list(Class<T> c);

	/**
	 * Update existing Student record in the database.
	 *
	 * @param id <T> Id to be found in database.
	 * @param c Class<T> to be searched for
	 * @return <T> object matching id parameter
	 * @throws EntityNotFoundException when there is no matching <T> record for id
	 */
	public T getById(Class<T> c, int id) throws EntityNotFoundException;

	/**
	 * Remove <T> record in the database.
	 *
	 * @param entity <T> Id to be removed from database.
	 */
	public void remove(T entity);

}

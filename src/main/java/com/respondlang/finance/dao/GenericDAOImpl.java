package com.respondlang.finance.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.respondlang.finance.model.Student;

public class GenericDAOImpl<T> implements GenericDAO<T> {

	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public void add(T entity) {
		entityManager.persist(entity);
	}

	@Override
	@Transactional
	public void update(T entity) {
		entityManager.merge(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> list(Class<T> c) {
		return (List<T>)entityManager.createQuery("FROM " + c.getSimpleName() , c).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getById(Class<T> c, int id) throws EntityNotFoundException {
		T entity = (T)entityManager.find(c,id);
		if (entity == null) {
			throw new EntityNotFoundException("Cannot find " + c.getSimpleName() +
					"for id: " + id);
		}

		return entity;
	}

	@Override
	@Transactional
	public void remove(T entity) {
		entityManager.remove(entity);
	}

}

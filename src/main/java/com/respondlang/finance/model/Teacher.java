package com.respondlang.finance.model;
// Generated 2017-11-05 15:09:33 by Hibernate Tools 5.2.5.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Teacher generated by hbm2java
 */
@Entity
@Table(name = "teacher", catalog = "finance")
public class Teacher implements java.io.Serializable {

	private Integer teacherId;
	private String firstName;
	private String lastName;
	private Set<StudentsGroup> studentsGroups = new HashSet<StudentsGroup>(0);

	public Teacher() {
	}

	public Teacher(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Teacher(String firstName, String lastName, Set<StudentsGroup> studentsGroups) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentsGroups = studentsGroups;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "teacher_id", unique = true, nullable = false)
	public Integer getTeacherId() {
		return this.teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	@Column(name = "first_name", nullable = false, length = 50)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name", nullable = false, length = 50)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher")
	public Set<StudentsGroup> getStudentsGroups() {
		return this.studentsGroups;
	}

	public void setStudentsGroups(Set<StudentsGroup> studentsGroups) {
		this.studentsGroups = studentsGroups;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((studentsGroups == null) ? 0 : studentsGroups.hashCode());
		result = prime * result + ((teacherId == null) ? 0 : teacherId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teacher other = (Teacher) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (studentsGroups == null) {
			if (other.studentsGroups != null)
				return false;
		} else if (!studentsGroups.equals(other.studentsGroups))
			return false;
		if (teacherId == null) {
			if (other.teacherId != null)
				return false;
		} else if (!teacherId.equals(other.teacherId))
			return false;
		return true;
	}

}
